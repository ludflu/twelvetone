## Play around with scales, triads and chord progressions

## to run:

`stack run`

```
Registering music-0.1.0.0...
[6,4,5,1]
[Prim (Note (1 % 2) (B,4)) :=: (Prim (Note (1 % 2) (D,5)) :=: (Prim (Note (1 % 2) (F,5)) :=: Prim (Rest (0 % 1)))),Prim (Note (1 % 2) (G,4)) :=: (Prim (Note (1 % 2) (B,4)) :=: (Prim (Note (1 % 2) (D,5)) :=: Prim (Rest (0 % 1)))),Prim (Note (1 % 2) (A,4)) :=: (Prim (Note (1 % 2) (C,5)) :=: (Prim (Note (1 % 2) (E,5)) :=: Prim (Rest (0 % 1)))),Prim (Note (1 % 2) (D,4)) :=: (Prim (Note (1 % 2) (F,4)) :=: (Prim (Note (1 % 2) (A,4)) :=: Prim (Rest (0 % 1))))]
[2,3,5,1]
[6,4,5,1]
[2,3,5,1]
```

## Dependencies:

[Haskell Stack](https://docs.haskellstack.org/en/stable/README/)

[Euterpea (Music composition in Haskell)](http://www.euterpea.com/faq/) 

Make sure you have a MIDI device available - on OSX you can install [SimpleSynth](http://notahat.com/simplesynth/) 

On linux try [Timididy](https://en.wikipedia.org/wiki/TiMidity%2B%2B) using the following command:

`timidity -iA -Os &`
