{-# LANGUAGE ScopedTypeVariables #-}
module Main where

import Scales
import Lib
import Euterpea
import System.Random

type Jnote = (Octave -> Dur -> Music Pitch)

mkMusicP :: Jnote -> Music Pitch
mkMusicP jn = jn 4 hn

notesFromPitches :: [Jnote] -> [Music Pitch]
notesFromPitches ns = map mkMusicP ns


mkScale :: [Jnote] -> [Music Pitch]
mkScale ns = let lscale = notesFromPitches ns
                 sp = shiftPitches 12  --shift up an octave
                 hscale = map (sp) lscale
              in lscale ++ hscale

-- take make a triad from a scale, take the root, 3rd and 5th
makeTriad :: [Music Pitch] -> [Music Pitch]
makeTriad ns = [(ns !! 0), (ns !! 2), (ns !! 4)] 

--given a scale and a degree, return the triad
moreTriads :: [Music Pitch] -> Int -> Int -> [Music Pitch]
moreTriads ns d c = let shifted = drop (d+c) ns
                     in makeTriad shifted


cm :: [Music Pitch]
cm = cmajor hn

sevenCM :: [Music Pitch]
sevenCM = sevenOctaves cm

main :: IO ()
main = let s = sevenCM
           t1 = moreTriads s 0 21
           t4 = moreTriads s 3 21
           t5 = moreTriads s 4 21
        in do playDev 0 $ chord t1
              playDev 0 $ chord t4
              playDev 0 $ chord t5
              playDev 0 $ chord t1

